import threading
import SocketServer
from camera import *
from sensor import *

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_LEFT_MOTOR, GPIO.OUT)  # left motor
GPIO.setup(GPIO_RIGHT_MOTOR, GPIO.OUT)  # right motor
GPIO.setup(GPIO_ECHO, GPIO.IN)  # echo
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)  # trigger


isObstacleAhead = False
thread_kill = False


def moveForward():
    GPIO.output(GPIO_LEFT_MOTOR, GPIO.HIGH)
    GPIO.output(GPIO_RIGHT_MOTOR, GPIO.HIGH)


def moveLeft():
    GPIO.output(GPIO_LEFT_MOTOR, GPIO.LOW)
    GPIO.output(GPIO_RIGHT_MOTOR, GPIO.HIGH)


def moveRight():
    GPIO.output(GPIO_LEFT_MOTOR, GPIO.HIGH)
    GPIO.output(GPIO_RIGHT_MOTOR, GPIO.LOW)


def stop():
    GPIO.output(GPIO_LEFT_MOTOR, GPIO.LOW)
    GPIO.output(GPIO_RIGHT_MOTOR, GPIO.LOW)


class CameraHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = True
        global thread_kill
        start = time.time()
        while not thread_kill and data:
            global isObstacleAhead
            data = self.request.recv(64)
            if time.time() - start < 10:  # 10 seconds for camera ready to show before car running
                continue
            if not isObstacleAhead:
                if data == sig.FORWARD:
                    moveForward()
                elif data == sig.LEFT:
                    moveLeft()
                elif data == sig.RIGHT:
                    moveRight()
                elif data == sig.STOP:
                    stop()
            else:
                stop()


def cameraHandling():
    cameraServer = SocketServer.TCPServer(('localhost', 8000), CameraHandler)
    cameraServer.serve_forever()


class SensorHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        global isObstacleAhead
        global thread_kill
        data = True
        while not thread_kill and data:
            data = self.request.recv(64)
            if data == sig.FORWARD:
                isObstacleAhead = False
            elif data == sig.STOP:
                print "Obstacle ahead"
                isObstacleAhead = True


def sensorHandling():
    sensorServer = SocketServer.TCPServer(('localhost', 8002), SensorHandler)
    sensorServer.serve_forever()

thread1 = threading.Thread(target=cameraHandling)
thread1.start()

thread2 = threading.Thread(target=sensorHandling)
thread2.start()

cameraProcessing = CameraProcessing()
thread3 = threading.Thread(target=cameraProcessing.run)
thread3.start()

sensorProcessing = SensorProcessing()
thread4 = threading.Thread(target=sensorProcessing.run)
thread4.start()

if raw_input() == 'q':
    thread_kill = True
    sensorProcessing.thread_kill = True
    cameraProcessing.thread_kill = True
stop()
