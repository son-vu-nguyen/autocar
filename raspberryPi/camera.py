import socket
#import struct
import time
from picamera import PiCamera
from picamera.array import PiRGBArray
#import sys
import cv2
import signalControl as sig


class CameraProcessing:
    def __init__(self):
        self.thread_kill = False

    def run(self):
        #stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #stream_socket.connect(('192.168.164.1', 8000))
        #connection = stream_socket.makefile('wb')

        driver_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        driver_socket.connect(("localhost", 8000))

        stopSign_cascade = cv2.CascadeClassifier('haarcascades/stop_sign.xml')
        trafficLight_cascade = cv2.CascadeClassifier('haarcascades/traffic_light.xml')

        try:
            camera = PiCamera()
            camera.resolution = (640, 480)
            camera.framerate = 32
            rawCapture = PiRGBArray(camera, size=(640, 480))
            time.sleep(2)                       # give 2 secs for camera to initilize

            for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
                frame = frame.array

                # Main processing
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                stops = stopSign_cascade.detectMultiScale(gray, 1.3, 4)
                lights = trafficLight_cascade.detectMultiScale(gray, 1.5, 5)
                for (x, y, w, h) in stops:
                    cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
                for (x, y, w, h) in lights:
                    cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)

                cv2.imshow("camera", frame)

                if self.thread_kill:
                    break
                if len(stops) > 0:
                    print "Stop sign ahead"
                    driver_socket.send(sig.STOP)
                    time.sleep(1)
                elif len(lights) > 0:
                    print "Red light ahead"
                    driver_socket.send(sig.STOP)
                    time.sleep(1)
                else:
                    # TODO: steering here
                    driver_socket.send(sig.FORWARD)
                

                ###### TODO: streaming
                #print frame
                #connection.write(frame)
                #stream_socket.send(frame)
                #print sys.getsizeof(frame)




                rawCapture.seek(0)
                rawCapture.truncate(0)
                # raw_input() : to test streaming frame by frame
                if cv2.waitKey(1000 / 12) & 0xff == ord("q"):
                    break
            cv2.destroyAllWindows()
        finally:
            driver_socket.close()
            #connection.close()
            #stream_socket.close()
