import RPi.GPIO as GPIO
import time
import socket
import signalControl as sig
from gpioPin import *


class SensorProcessing:
    def __init__(self):
        self.thread_kill = False

    def measure(self):
        # measure distance
        GPIO.output(GPIO_TRIGGER, True)
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER, False)
        start = time.time()
        stop = start

        while GPIO.input(GPIO_ECHO) == 0:
            start = time.time()

        while GPIO.input(GPIO_ECHO) == 1:
            stop = time.time()

        elapsed = stop-start
        distance = (elapsed * 34300)/2

        return distance

    def run(self):
        driver_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        driver_socket.connect(("localhost", 8002))

        # initialize trigger pin to low
        GPIO.output(GPIO_TRIGGER, False)

        while True:
            distance = self.measure()
            print "Distance : %.1f cm" % distance
            if self.thread_kill:
                break
            if distance < 50:
                driver_socket.send(sig.STOP)
            else:
                driver_socket.send(sig.FORWARD)
            time.sleep(0.2)
        driver_socket.close()
