import RPi.GPIO as GPIO
from gpioPin import *

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_LEFT_MOTOR, GPIO.OUT)  # left motor
GPIO.setup(GPIO_RIGHT_MOTOR, GPIO.OUT)  # right motor

GPIO.output(GPIO_LEFT_MOTOR, False)
GPIO.output(GPIO_RIGHT_MOTOR, False)
