import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)

# define pi GPIO
GPIO_TRIGGER = 22
GPIO_ECHO    = 17

GPIO.setmode(GPIO.BCM)  
GPIO.setup(GPIO_ECHO, GPIO.IN) #echo
GPIO.setup(GPIO_TRIGGER, GPIO.OUT) #trigger

# initialize trigger pin to low
GPIO.output(GPIO_TRIGGER, False)

def measure():
  # measure distance
  GPIO.output(GPIO_TRIGGER, True)
  time.sleep(0.00001)
  GPIO.output(GPIO_TRIGGER, False)
  start = time.time()

  while GPIO.input(GPIO_ECHO) == 0:
    start = time.time()

  while GPIO.input(GPIO_ECHO) == 1:
    stop = time.time()

  elapsed = stop-start
  distance = (elapsed * 34300)/2

  return distance

while True:
    distance = measure()
    print "Distance : %.1f cm" % distance
    # send data to the host every 0.5 sec
    time.sleep(0.5)
#GPIO.cleanup()
