from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2

camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
time.sleep(0.1)


def detectFromCamera():
    trafficLight_cascade = cv2.CascadeClassifier('haarcascades/traffic_light.xml')

    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        frame = frame.array
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        lights = trafficLight_cascade.detectMultiScale(gray, 1.3, 3)
        # print len(lights)  ### when len > 0 => there are red light ahead
        for (x, y, w, h) in lights:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 2)
        cv2.imshow("camera", frame)
        rawCapture.truncate(0)
        if cv2.waitKey(1000 / 12) & 0xff == ord("q"):
            break
    cv2.destroyAllWindows()

detectFromCamera()
