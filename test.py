import threading
import SocketServer
import time
import socket
import numpy as np
import struct
import random


def randomList():
    list = []
    for i in range(0,2):
        li = []
        for j in range(0,3):
            lj = []
            for k in range(0,3):
                lj.append(random.randint(0,255))
            li.append(lj)
        list.append(li)
    return list


class DataHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        data = True
        while data:
            data = self.request.recv(1024)
            if len(data) == 0:
                break
            frame = struct.unpack('18i', data)
            frame = np.array(frame).reshape((2,3,3))
            print "Server receive array " + str(frame)


def test():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        client.connect(("localhost", 8000))
        i = 1
        while i < 5:
            frame = np.array(randomList())
            print "Client sending array " + str(frame)
            frame = frame.flatten()
            decode = struct.pack('18i', *frame)
            client.send(decode)
            i += 1
    finally:
        client.close()


def server():
    server = SocketServer.TCPServer(('', 8000), DataHandler)
    server.serve_forever()


thread1 = threading.Thread(target=server)
thread1.start()
time.sleep(5)
thread = threading.Thread(target=test)
thread.start()